#!/bin/bash
NAMESPACE=${1:-nexus}

kubectl create secret tls secure-tls --key ./registry.wakezzz.com.key --cert ./registry.wakezzz.com.crt --namespace=$NAMESPACE
kubectl apply -f 02_nexus-pvc.yaml --namespace=$NAMESPACE
kubectl apply -f 03_nexus-deployment.yaml --namespace=$NAMESPACE
kubectl apply -f 04_nexus-service.yaml --namespace=$NAMESPACE
kubectl apply -f 05_nexus-ingress.yaml --namespace=$NAMESPACE
