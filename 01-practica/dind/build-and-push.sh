#!/bin/bash

ARG1=${1:-19.03.4-dind-0.0.2}

docker build -t ureure/ure-dind-nexus:$ARG1 .
docker push ureure/ure-dind-nexus:$ARG1
