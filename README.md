# Práctica Módulo 06 - Ciclo de Vida de un Desarrollo CI/CD

Proyecto académico con el objetivo de utilizar una arquitectura en Google Cloud Platform que ponga de manifiesto las buenas prácticas del CI/CD, ([enunciado](./Enunciado-Practica_Ciclo_Desarrollo_CI_CD.pdf)).

## Instalación de requisitos

Se ha comprobado la ejecución correcta de los ejercicios en un sistema operativo [Debian 9](https://www.debian.org/index.es.html), con las siguientes aplicaciones previamente instaladas:

* [Google Cloud SDK](https://cloud.google.com/sdk/install) 264.0.0.
  * alpha 2019.09.22.
  * beta 2019.0.22.
  * bq 2.0.47.
  * core 2019.09.22.
  * gsutil 4.42.
  * kubectl 2019.09.22.
* [Docker](https://docs.docker.com/install/linux/docker-ce/debian/) 19.03.2, build 6a30dfca03.
* [Docker Compose](https://docs.docker.com/compose/install/) 1.24.1, build 4667896b.
* [Git](https://www.git-scm.com/) 2.11.0.

Instalar "*Google Cloud SDK*":

```bash
export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"
echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo apt-get update && sudo apt-get install google-cloud-sdk -y
```

![Instalar Google Cloud SDK](./00-requisitos/img/instalar-gcloud-01.png)

Inicializar el SKD "*Google Cloud SDK*":

```bash
gcloud init
```

![Inicializar Google Cloud SDK 1](./00-requisitos/img/inicializar-gcloud-01.png)

Hacer login con la guenta de Google:

![Inicializar Google Cloud SDK 2](./00-requisitos/img/inicializar-gcloud-02.png)

![Inicializar Google Cloud SDK 3](./00-requisitos/img/inicializar-gcloud-03.png)

Copiar el código obtenido en la consola:

![Inicializar Google Cloud SDK 4](./00-requisitos/img/inicializar-gcloud-04.png)

Seleccionar el proyecto "*practica-kc-06-ciclodesa*":

![Inicializar Google Cloud SDK 5](./00-requisitos/img/inicializar-gcloud-05.png)

Escoger como region por defecto la *17* ("*europe-west1-b*"):

![Inicializar Google Cloud SDK 6](./00-requisitos/img/inicializar-gcloud-06.png)

![Inicializar Google Cloud SDK 7](./00-requisitos/img/inicializar-gcloud-07.png)

Comprobar la instalación del SKD "*Google Cloud SDK*":

```bash
gcloud --version
```

![Comprobar Google Cloud SDK](./00-requisitos/img/comprobar-gcloud-01.png)

Instalar *[kubectl](https://kubernetes.io/es/docs/tasks/tools/install-kubectl/)*:

```bash
sudo apt-get install kubectl -y
```

![Instalar kubectl](./00-requisitos/img/instalar-kubectl-01.png)

Instalar "*Docker*" añadiendo nuestro usuario (en el ejemplo "devops") al grupo "docker":

```bash
sudo apt-get install apt-transport-https ca-certificates curl gnupg2 software-properties-common -y
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install docker-ce -y
sudo usermod -aG docker devops
exit
```

Comprobar la instalación de Docker:

```bash
docker --version
```

![Comprobar Docker](./00-requisitos/img/comprobar-docker-01.png)

Instalar "*Docker Compose*":

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
```

![Instalar Docker Compose](./00-requisitos/img/instalar-docker-compose-01.png)

Instalar "*Git*":

```bash
sudo apt-get install git -y
```

![Instalar Git](./00-requisitos/img/instalar-git-01.png)

Dar acceso completo al proyecto "*practica-kc-06-ciclodesa*" al usuario "cgarciaarano@gmail.com", con rol "*editor*":

```bash
gcloud projects add-iam-policy-binding practica-kc-06-ciclodesa --member user:cgarciaarano@gmail.com --role roles/editor
```

![Dar acceso proyecto](./00-requisitos/img/dar-acceso-proyecto-editor-01.png)

## Crear clúster en GKE con la infraestructura necesaria

Clonar este repositorio en el directorio "*practica*":

```bash
mkdir practica
cd practica
git clone https://gitlab.com/aureliopn/06_ciclodesa.git
cd ..
```

![Clonar repositorio Git práctica](./01-practica/img/clonar-repositorio-01.png)

Ejecutar el script [crear_cluster_practica.sh](./01-practica/crear_cluster_practica.sh) que crea el clúster con la práctica instalada:

```bash
cd practica/ciclodesa/01-practica
sudo chmod 700 *.sh
./crear_cluster_practica.sh >> log-crear-cluster.txt
cd ../../..
```

![Crear clúster](./01-practica/img/crear-cluster-01.png)

En caso de encontrar algún problema, se puede estudiar la salida generada en el archivo [log-crear-cluster.txt](./01-practica/log-crear-cluster.txt). En él, también aparece la contraseña inicial del usuario "admin":

```bash
NOTES:

Your GitLab Runner should now be registered against the GitLab instance reachable at: "https://gitlab.keepcoding.io/"
namespace/nexus created
secret/secure-tls created
persistentvolumeclaim/nexus-pv-claim created
deployment.apps/nexus created
service/nexus created
ingress.extensions/nexus created
Password admin Nexus: 541ac999-8365-44c4-8fae-fb39468fbe70
```

El script, además de crear un clúster, realiza la instalación de:
* Una instancia de [gitlab-runner](https://docs.gitlab.com/runner/) enlazada con [nuestro proyecto de la aplicación en GitLab](https://gitlab.com/aureliopn/06_practica-ci).
* Una instancia de [Sonatype Nexus](https://www.sonatype.com/product-nexus-repository) con los siguientes puntos de entrada:
  * Administración web: http://nexus.34.76.198.42.nip.io/. (Varía en función de la IP asignada al "Load Balancer").
  * Servidor Registry: http://registry.wakezzz.com/. (Requiere apuntar el subdominio a la IP asginada al "Load Balancer").

El archivo [values-gitlab-runner.yaml](./01-practica/gitlab-runner/values-gitlab-runner.yaml) contiene el token para unirse al proyecto de GitLab:

![Gitlab runner](./01-practica/img/gitlab-runner-01.png)

Tras la instalación de Sonatype Nexus, se realizó manualmente la creación de un un repositorio "Docker Hosted" (Docker Registry):

![Sonatype Nexus repositorio](./01-practica/img/sonatype-nexus-01.png)

Además, se realizó manualmente la creación del usuario "ure" para los procesos ejecutados por nuestro gitlab-runner.

![Sonatype Nexus usuario](./01-practica/img/sonatype-nexus-02.png)

Finalmente, las credenciales de nuestro Sonatype Nexus son:
* Usuario administrador:
  * admin
  * nexus-kc-06
* Usuario servicio Registry:
  * ure
  * ureureure

## Ejecuciones locales de la aplicación y de las pruebas

Clonar el repositorio de la aplicación en el directorio "*practica*":

```bash
cd practica
git clone https://gitlab.com/aureliopn/06_practica-ci
cd ..
```

![Clonar repositorio Git aplicación](./01-practica/img/clonar-repositorio-02.png)

Crear imagen base:

```bash
cd practica/practica-ci/
docker build -t desatranques/base . >> log-build-base.txt
cd ../..
```

![Crear imagen base](./01-practica/img/crear-imagen-base-01.png)

En caso de encontrar algún problema, se puede estudiar la salida generada en el archivo [log-build-base.txt](./01-practica/log-build-base.txt).

Ejecución local:

```bash
cd practica/practica-ci/
docker-compose up >> log-compose-up.txt
```

Utilizar "Ctrl+C" para terminar. Después, eliminar los recursos construidos:

```bash
docker-compose down
cd ../..
```

![Levantar Docker compose](./01-practica/img/levantar-docker-compose-01.png)

En caso de encontrar algún problema, se puede estudiar la salida generada en el archivo [log-compose-up.txt](./01-practica/log-compose-up.txt).

Ejecución local con pruebas:

```bash
cd practica/practica-ci/
docker-compose -f docker-compose.yml -f test/docker-compose.yml build >> log-compose-test-build.txt
docker-compose -f docker-compose.yml -f test/docker-compose.yml run tester >> log-compose-test-run.txt
docker-compose -f docker-compose.yml -f test/docker-compose.yml down
cd ../..
```

![Levantar Docker compose test](./01-practica/img/levantar-docker-compose-test-01.png)

En caso de encontrar algún problema, se puede estudiar la salida generada en los archivos [log-compose-test-build.txt](./01-practica/log-compose-test-build.txt) y [log-compose-test-run.txt](./01-practica/log-compose-test-run.txt).

## Integración Continua

### GitLab CI

Se ha utilizado [GitLab CI](https://about.gitlab.com/product/continuous-integration/) atendiendo a que:
* El repositorio de la aplicación ya está en GitLab, la integración será más fácil que con [Jenkins](https://jenkins.io/).
* Los requerimientos son ampliamente cubiertos con una configuración mínima del [gitlab-runner](./01-practica/gitlab-runner/values-gitlab-runner.yaml).

Para las ejecuciones del pipeline de CI, se utiliza la imagen Docker [ureure/ure-dind-nexus:19.03.4-dind-0.0.1](https://hub.docker.com/r/ureure/ure-dind-nexus). El [Dockerfile](./01-practica/dind/Dockerfile) con el que se construye, incorpora:
* El certificado del servidor Registry de nuestro clúster, que ha sido utilizado en el objeto [ingress](./01-practica/nexus/05_nexus-ingress.yaml) de Kubernetes.
* El punto de entrada [dind-opts.sh](./01-practica/dind/dind-opts.sh), que puede resultar necesario si se desean inyectar parámetros al "dockerd-entrypoint.sh" de la imagen base (actualmente, no lo hace).
* Docker-compose para la ejecución de las pruebas de la aplicación.

### Versionado

Los artefactos se versionan utilizando el "*tag*" de la imagen que se sube al repositorio. Hay dos tipos de valores que se asignan al "*tag*" del artefacto:
* Versión temporal: hash del commit + "_tmp"; por ejemplo "c7a2a96f55c22b3820d7e96665108743daffb9af_tmp".
* Versión definitiva:
  * Rama "release": versión de la rama; por ejemplo, para la rama "release:2.3.1", la versión es "2.3.1".
  * Ramas "master" y "feature": hash del commit; por ejemplo "c7a2a96f55c22b3820d7e96665108743daffb9af" (sin la cadena "_tmp").

```yaml
...
  before_script:
    ...
    - if [[ $CI_COMMIT_REF_NAME == release* ]]; then export VERSION="${CI_COMMIT_REF_NAME#*/}"; else export VERSION="${CI_COMMIT_SHA}"; fi
...
```

El valor del "*tag*" se establece con el siguiente criterio según las imágenes generadas:
* La imagen "*base*" siempre utiliza la versión definitiva.
* La imagen "*test*" siempre utiliza la versión temporal.
* Las imágenes de los artefactos "gateway", "orders" y "products", utilizan:
  * La versión temporal antes de la etapa "*publish*".
  * La versión definitiva en la etapa "*publish*".

Ejemplo de artefactos generados en pipelines de ramas "release" y "master":

![Versionado](./01-practica/img/versionado-01.png)

### Pipeline "*.gitlab-ci.yml*"

Cada etapa del [pipeline](https://gitlab.com/aureliopn/06_practica-ci/blob/master/.gitlab-ci.yml) se ejecuta si no falló ninguna etapa anterior. Las etapas del pipeline se ejecutan secuencialmente, en el siguiente orden:
* *build-base*, ejecuta el trabajo:
  * Construir la imagen "*base*" del resto de artefactos, y guardarla en el repositorio con su versión definitiva.
* *build-modules*, ejecuta los trabajos en paralelo:
  * *build-gateway*: construir artefacto "gateway" extendiendo la imagen "*base*", y guardarlo en el repositorio con su versión temporal.
  * *build-orders*: construir artefacto "orders" extendiendo la imagen "*base*", y guardarlo en el repositorio con su versión temporal.
  * *build-products*: construir artefacto "products" extendiendo la imagen "*base*", y guardarlo en el repositorio con su versión temporal.
  * *build-tester*: construir la imagen "*test*" extendiendo la imagen "*base*", y guardarla en el repositorio con su versión temporal.
* *test*, ejecuta los trabajos en paralelo:
  * *test-gateway*: ejecutar las pruebas unitarias de la versión temporal del artefacto "gateway".
  * *test-orders*: ejecutar las pruebas unitarias del la versión temporal del artefacto "orders".
  * *test-products*: ejecutar las pruebas unitarias del la versión temporal del artefacto "products".
* *publish*, ejecuta los trabajos en paralelo:
  * *publish-gateway*: publica del artefacto "gateway" con su versión definitiva.
  * *publish-orders*: publica del artefacto "orders" con su versión definitiva.
  * *publish-products*: publica del artefacto "products" con su versión definitiva.

Ejemplo de ejecución de pipeline en rama "release/0.0.1":

![Ejecución pipeline](./01-practica/img/ejecucion-pipeline-01.png)

Los trabajos de cada etapa se ejecutan cumpliendo los siguientes criterios:
* Rama release:
  * Se ejecutan todos los trabajos de todas las etapas.
* Ramas "master" y "feature":
  * Los trabajos *build-base* y *build-tester*, se deben ejecutar si hay cambios en:
    * Dockerfile
    * requirements.txt
    * .coveragerc
    * gateway/**/*
    * orders/**/*
    * products/**/*
    * test/**/*
  * Los trabajos *build-gateway*, *test-gateway* y *publish-gateway*, se deben ejecutar si hay cambios en:
    * Dockerfile
    * requirements.txt
    * .coveragerc
    * gateway/**/*
    * test/**/*
  * Los trabajos *build-orders*, *test-orders* y *publish-orders*, se deben ejecutar si hay cambios en:
    * Dockerfile
    * requirements.txt
    * .coveragerc
    * orders/**/*
    * test/**/*
  * Los trabajos *build-products*, *test-products* y *publish-products*, se deben ejecutar si hay cambios en:
    * Dockerfile
    * requirements.txt
    * .coveragerc
    * products/**/*
    * test/**/*

Ejemplo de ejecución de pipeline en rama "feature/listar-productos-disponibles":

![Ejecución pipeline](./01-practica/img/ejecucion-pipeline-lpd-01.png)

## Problemas conocidos

### Docker Registry HTTP

Escenario:
* Servidor Registry HTTP.
* Ejecución de "dockerd-entrypoint.sh" con la opción "--insecure-registry" en [dind-opts.sh](./01-practica/dind/dind-opts.sh).
* Variable *DOCKER_TLS_CERTDIR: ""* en pipeline.

Se produce el error "*registry x509 certificate signed by unknown authority*", **aleatoriamente**, en la instrucción del pipeline:

```bash
echo $DOCKER_REGISTRY_PASSWORD | docker login -u $DOCKER_REGISTRY_USER --password-stdin $DOCKER_REGISTRY
```

El problema se evita con un Docker Registry HTTPS:
* Se utilizó [Let's Encrypt](https://letsencrypt.org/es/), con el subdominio "registry.wakezzz.com" apuntando a la IP del "Load Balancer" (del clúster de Kubernetes en GKE).
* Se utilizó el certificado generado con [certbot](https://certbot.eff.org/) en:
  * El script [start-nexus.sh](./01-practica/nexus/start-nexus.sh), para desplegar Sonatype Nexus.
  * El [Dockerfile](./01-practica/dind/Dockerfile) que genera la imagen Docker [ureure/ure-dind-nexus:19.03.4-dind-0.0.1](https://hub.docker.com/r/ureure/ure-dind-nexus).

### Docker Registry Ingress

Se produce el error "*413 Request Entity Too Large*" en la instrucción del pipeline:

```bash
docker push $DOCKER_IMAGE_PROJECT/base:$VERSION
```

El motivo es que se excede el límite del tamaño del "*body*" en las peticiones. El problema se corrige añadiendo en [05_nexus-ingress.yaml](./01-practica/nexus/05_nexus-ingress.yaml) la línea:

```yaml
nginx.ingress.kubernetes.io/proxy-body-size: 900m
```

### Repositorio Sonatype Nexus

El espacio libre mínimo para los repositorios de Sonatype Nexus está configurado por defecto en 4 GB; con menos espacio, funciona en modo de sólo lectura. Para evitar la falta de espacio libre, se aumentó a 100 GB el tamaño del PVC utilizado en [02_nexus-pvc.yaml](./01-practica/nexus/02_nexus-pvc.yaml).

Sería conveniente configurar una política de borrado de artefactos. Por ejemplo, borrar las imágenes que cumplan tener:
* Un "*tag*" que termine en "_tmp".
* Más de dos días de antigüedad.

### Usuario Sonatype Nexus

Se está utilizando un usuario con rol de administrador para ejecutar "*pull*" y "*push*" en el Docker Registry, sería conveniente que el usuario tenga un rol con los mínimos privilegios necesarios.
